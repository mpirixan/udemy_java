package model.services;

public class PayPalService implements OnlinePaymentService{

		public Double interest(Double amount, Integer months) {
			return (amount * 0.1) * months; 
		}
		
		public Double paymentFee(Double amount) {
			return amount * 0.2;
		}
	
}
